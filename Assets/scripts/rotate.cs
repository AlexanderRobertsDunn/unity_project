﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate : MonoBehaviour {
    private float initTime;
    private int rotationCount;
    public UnityEngine.UI.Text rotationText;

	// Use this for initialization
	void Start () {
        initTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        for (float i = 0; i < 361; i++)
        {
            //gameObject.transform.Rotate(1, i, 0);
            gameObject.transform.Rotate(0, 0, (i*0.1f/2));
            
            if(i == 360)
            {
                this.rotationText.text = "Rotation: " + rotationCount.ToString();
                //this.rotationText;
            }
        }
    }
}
